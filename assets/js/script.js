
/*
	Create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.

	Add the new course into the array and show the following alert:
		"You have created <nameOfCourse>. The price is <priceOfCourse>."

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
 */



/*
Create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.

*/
let arrCourse = [];

const postCourse = (id, name, description, price, isActive) => {
	arrCourse.push({id, name, description, price, isActive});
	alert (`You have created ${name}. The price is ${price}`);
}

postCourse ('SCI','Science', 'about science', 100, true);
postCourse ('MTH','Math', 'about math', 150, false);
postCourse ('MTH','Math', 'about math', 150, false);
console.log(arrCourse);

/*
Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()
*/

const findCourse = (array, id) => {
	const find = array.find(element => id);
	console.log(`${id} found`);
	return find;
}

findCourse(arrCourse, 'SCI');

/*
Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
*/

const deleteCourse = (array) => {
	array.pop();
	console.log(`Course deleted`)
};

deleteCourse(arrCourse);
console.log(arrCourse);